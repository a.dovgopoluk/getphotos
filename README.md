## Краткое описание

Данное приложение принимает на вход ссылку и выводит список изображений из полученного HTML
Элемент списка содержит URL изображения, разрешения изображения и размер в килобайтах, а также его предпросмотр. Ссылку необходимо указать в полном формате (https://...)

Проект написан на фреймворках Node js и React

Данный проект является тестовым заданием

## Инструкция по запуску.

Для запуска Вам понадобится Node js: https://nodejs.org/ru/

После установки сколинируйте текущий репозиторий.
Существует да способа
    
1. Скачать архив и распаковать в любую папку    
2. Используя Git Bush и команду git clone. Для windows:
   
   -Открыть Git Bush в папке, в которой Вы хотите запустить проект
   
   -Написать команду: git clone https://gitlab.com/a.dovgopoluk/getphotos

Перейдите в папку ./getphotos/server

Откройте командную строку (терминал) и напишите команду:
### npm install
После успешной установки пакетов выполните команду:
### npm run start
Приложение успешно запущено. Перейдите на страницу для лоступа к функционалу:
### http://localhost:5000/


## Brief description

This app gets a http link on input and gives the list of images from the html document
List item contains image URL, resolution, size (in Kb) and image preview. The link must be full format (https://...)

The source code of this app uses Node js and React frameworks.

This project is a test assignment

## Start-up instruction

You need to install Node js to run this app: https://nodejs.org/en/

After the installation clone this repository.
There are two ways to do this:

1. Download the archive and unpack it in any directory
2. Use Git Bush. On Windows:
    -Open Git Bush in directory you want
    -Write the command: git clone https://gitlab.com/a.dovgopoluk/getphotos
    
Go to the folder ./getphotos/server

Open cmd or terminal and write:
### npm install
After the installation execute:
### npm run start
The app has started. Go to this link to use all the functions:
### http://localhost:5000/