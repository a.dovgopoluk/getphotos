var express = require("express");
var router = express.Router();
var fetch = require("node-fetch")
var DomParser = require('dom-parser');
var probe = require('probe-image-size');
var parser = new DomParser();

router.post("/img", async (req, res) =>{
    try{
        const {url} = req.body
        console.log(url)
        const response = await fetch(url)
            .then(res => res.text())

        const html = parser.parseFromString(response);
        res.send(html);
    }
    catch (e) {
        console.log("Error: ", e.message)
        await res.status(500).json("An error occurred: " + e.message);
    }
})

const imgToMeta = (imgs) => {
    return Promise.all(imgs.map(async img => {
        const meta = await fetch(img.ref)
            .then(function (response) {
                return response.blob()
            })
            .then(function (blob) {
                return { size: blob.size }
            });
        const meta2 = await probe(img.ref).then(({ width, height }) => {
            return { width, height }
        });
        return ({ ...meta, ...meta2, img: img.ref, alt: img.alt })
    }))
}

router.post("/meta", async (req, res) =>{
    try{
        const {imgs} = req.body
        console.log("Getting meta")
        const response = await imgToMeta([...imgs])
        console.log("Response gotten");

        res.send(response);
    }
    catch (e) {
        console.log("Error: ", e.message)
        await res.status(500).json("An error occurred: " + e.message);
    }
})

module.exports = router