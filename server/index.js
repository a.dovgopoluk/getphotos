const express = require("express")
const app = express()

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/", require("./routes"))

app.use(express.static("public"));//!!!

app.use('*', function(req, res) {
    res.sendFile(__dirname +'/public/index.html');
});

app.listen(5000)