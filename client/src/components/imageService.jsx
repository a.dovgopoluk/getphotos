import uParser from "url";
import React from 'react';


export const getMeta = async (imgs) => {
    const body = JSON.stringify({imgs})
    const headers = {}
    headers['Content-Type'] = 'application/json'
    const response = await fetch('/meta', {
        method: "post",
        body,
        headers
    })
    if (response.ok) {
        const data = await response.json()
        return data;
    }
    else {
        throw new Error('Failed to get Meta')
    }
}

export const getHTML = async (url) => {
    const body = JSON.stringify({url})
    const headers = {}
    headers['Content-Type'] = 'application/json'
    const response = await fetch('/img', {
        method: "post",
        body,
        headers
    })
    if (response.ok) {
        const data = await response.json()
        // console.log(data.rawHTML)
        return data.rawHTML;
    }
    else {
        throw new Error('Failed to get HTML')
    }
}

export const docToRef = (doc, url) =>{
    const urlObj = uParser.parse(url)
    const imgs = []
    const temp = [...doc.images].forEach(img =>{
        if (!img.src) return
        const imgUrl = uParser.parse(img.src)
        if (imgUrl.hostname !== "localhost"){
            imgs.push({ref: img.src, alt: img.alt})
            return
        }
        imgUrl.href = imgUrl.href.replace(imgUrl.host, urlObj.host)
        imgUrl.href = imgUrl.href.replace(imgUrl.protocol, urlObj.protocol)
        imgs.push({ref: imgUrl.href, alt: img.alt})
    })
    if (imgs.length === 0) throw new Error("No img")
    // console.log(imgs)
    return imgs
}