import React, { Component } from 'react';
import { DataScroller } from 'primereact/datascroller';
import { ProgressSpinner } from 'primereact/progressspinner';
import './ImgScroller.scss'

class ImgScroller extends Component {
    constructor(props) {
        super(props);

    }

    cardTemplate(data) {
        return (
            <div className="image-item">
                <img src={data.img} alt={data.alt}/>
                <div className="image-meta">
                    <div className="image-link">
                        <a href={data.img} className="image-ref" >{data.img}</a>
                            {/*{data.img}*/}
                    </div>
                    <div className="image-info">
                        <div className="image-size">Size: {(data.size / 1024).toFixed(2)}Kb</div>
                        <div className="image-width">Width: {data.width}</div>
                        <div className="image-height">Height: {data.height}</div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const card = this.props.meta.length === 0
            ? <ProgressSpinner />
            : <DataScroller value={this.props.meta} itemTemplate={this.cardTemplate} rows={5} inline scrollHeight="600px" header="Scroll Down to Load More" />

        return (
            <div className="datascroller">
                <div className="card">
                    {card}
                </div>
            </div>
        );
    }
}

export default ImgScroller