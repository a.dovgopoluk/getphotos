import React, {useState, useEffect, useCallback} from 'react';
import './App.css';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import uParser from 'url'
import ImgScroller from './components/imgScroller/ImgScroller'
import {getMeta, getHTML, docToRef} from "./components/imageService";

function App() {
    const [url, setURL] = useState("");
    const [output, setOutput] = useState();
    const [doc, setDoc] = useState();
    const [imgs, setImgs] = useState([]);
    const [meta, setMeta] = useState([]);
    const [trigger, setTrigger] = useState(false)
    const [errTrigger, setErrTrigger] = useState(false)

    // преобразовать текст в HTML
    useEffect(()=>{
        if(!output) return
        const tempDoc = new DOMParser().parseFromString(output, "text/html");  // преобразовать текст в HTML
        setDoc(tempDoc);
    }, [output])

    useEffect(() =>{
        try{
            if (doc){
                setImgs(docToRef(doc, url))
            }
        }
        catch (e) {
            console.log(e)
            setErrTrigger(true)
        }
    },[doc])

    const setHTML = async (url) => {
        setOutput(await getHTML(url).catch(err => {
            setErrTrigger(true)
        }));
    }

    useEffect(() => {
        if (imgs.length === 0) return
        setMetaData(imgs)
    }, [imgs])

    const setMetaData = async (imgs) => {
        setMeta(await getMeta(imgs).catch(err => {
            setErrTrigger(true)
        }));
    }

    useEffect(() => {
        if(errTrigger){
            setMeta([])
            setTrigger(false)
        }
    }, [errTrigger])

    const clickHandler = async (e) =>{
        e.preventDefault();
        setErrTrigger(false)
        setMeta([]);
        setTrigger(true)
        await setHTML(url)
    };

    return (
        <div className="App">
            <div className="URL">
                  <h3>Type the link</h3>
                  <InputText value={url} onChange={(e) => setURL(e.target.value)} />
                  <Button label="Submit" onClick={(e)=>clickHandler(e)} className=".p-button-secondary"/>
            </div>
            <div className="Data">
                {trigger && <ImgScroller meta={meta}/>}
            </div>
            <div className="err-handler">
                {errTrigger && "Error!"}
            </div>
        </div>
  );
}

export default App;
